
suit = require "SUIT"
HC = require "HC"
gamestate = require "hump.gamestate"
vector = require "hump.vector"

require "util"


-- Gamestates
SMain = require "SMain"
SGame = require "SGame"


-- Useful aliases and "Constants"
width = love.graphics.getWidth
height = love.graphics.getHeight

Diagonal_Length = math.sqrt(math.pow(width(), 2) + math.pow(height(), 2))

SUIT_Cell_Width = width() / 5
SUIT_Cell_Heigth = height() / 16

-- print(SUIT_Cell_Width, SUIT_Cell_Heigth)

-- TODO: put some better place, but have it here for now
function SUITLayoutInitial()
  suit.layout:reset()
  suit.layout:row(SUIT_Cell_Width, SUIT_Cell_Heigth)
end


function love.load(arg)
  math.randomseed( os.time() )

  gamestate.registerEvents() -- modifies all of loves callbacks to include calls to gamestates callbacks!
  gamestate.switch(SMain)
end

function love.update(dt)
end

function love.draw()
end
