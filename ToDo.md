
- [X] Add GUI (SUIT)
- [X] Add utility stuff (HUMP)
  - [#] Add gamestate
  - [X] Add state: main menu
  - [X] Add state: game
  - [ ] Add state: pause
- [X] Add a player
  - [X] Player movement
  <!-- - [ ] Player shooting (Will not be added in this "edition")-->
- [X] Add enemies
  - [X] Enemies chasing
  - [ ] Indicator for offscreen Enemies
  <!-- - [ ] Enemies shooting(Will not be added in this "edition")-->
- [X] Add collision detection (HC library)
  - [X] Detect collisions between player and enemy
  - [X] Player loss when touched by enemy
- [ ] Restrict playfield to specific area (NOT screen!)
  - [ ] Put GUI elements in relation to this


Hm... Gonna put this more in a priority feature list I think
These should be seen as minimal increments, that are prioritized from
highest impact on game experience (or importance of function(?)) to lowest.

Numbered sub items are treated as the same feature as sub items with the same
number within that root item. (sub items with a higher number often denote
specific improvements on the sub items with lower numbers, such as:
 1. "Feature mechanics/functionality"
 2. "GUI for feature"
 3. "more fancy GUI for feature"
for example)

For non-ordered sets of sub-items, I'll just use letters or something. Same
(non-digit) symbol for several sub items means they are grouped.

Commits should be made for at least every feature implemented (AND TESTED!).

Hm, how to keep track of "split" order of priority?
Currently some sub items are lower priority than some root items technically, but
are nested inside higher priority root items...
For now I'm just going to move the items around as the priorities change.

- [ ] Add pause state
      While each round is short, not having a proper pause screen just doesn't
      feel right. Can open up avenues for cheating, so should probably have the
      "overlay" be completely opaque, so that pausing is more a disadvantage
  - [ ] 1. Keyboard button to toggle game update
  - [ ] 2. GUI Button to return to game on pause screen
  - [ ] 2. GUI Button to return to main menu on pause screen
  - [ ] 3. Create "overlay" (Darken elements that are "below" the pause "screen")

- [ ] Constrain game area to some "play area", not the screen.
      Makes it a _lot_ easier to fix balance. Can remove score off of the play
      area. Can make sure all players have the same difficulty (scale play field
      according to screen size). Just all in all a good distinction to make.
  - [ ] 1. Change current size calculations to go by self made variables instead of width()/height()
  - [ ] 2. Make area square
  - [ ] 2. Make sure play area and game elements always have the same proportions
  - [ ] 3. Draw box around area and make is smaller than screen
  - [ ] 3. Put GUI elements in relation to play area (score)

- [ ] Add indicator for "off screen" Enemies
      Make sure player doesn't get surprised, this is a game of skill, not
      luck.
  - [ ] 1. Add some circle that hugs the sides of the screen.
  - [ ] 2. Improve graphics
  - [ ] 3. Have the indicator change depending on distance (size)

- [ ] Improve GUI
  - [ ] a. Get a new font
  - [ ] a. Increase size of Title and Game Over
  - [ ] a. Improve the layouting
  - [ ] b. Better colors(?)

- [ ] Factor out Entity as a superclass of Player and Enemy
      Maybe look into component-based "layout"?
      Probably better for when I have several types of Enemies, but I'll think
      about it.




List of finished features (for tracking what has been done and in what order)
- Set title to "Circumlocute"
  (Make sure all references to the game name are changed)
