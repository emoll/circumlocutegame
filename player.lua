
Player = {}
attachEmptyLoveCallbacks(Player) -- bit of extra "work" since I redefine functions but eh

function Player:new(obj)
  obj = obj or {}

  setmetatable(obj, self)
  self.__index = self


  obj.dims = obj.dims or vector(50, 50)

  obj.pos = obj.pos or vector(0, 0)
  obj.speed = obj.speed or 200 -- px/s (?)

  obj.color = obj.color or {255, 255, 255}

  obj.colRect = HC.rectangle(obj.pos.x, obj.pos.y, obj.dims.x, obj.dims.y)


  return obj
end


function Player:setPosition (x, y)
  if vector.isvector(x) then
    self.pos = x
  else
    self.pos.x = x
    self.pos.y = y
  end

  self.colRect:moveTo(self.pos.x, self.pos.y)
end


function Player:move(dt)
  local dir = moveDirFromKeys(
    love.keyboard.isDown("w"),
    love.keyboard.isDown("s"),
    love.keyboard.isDown("a"),
    love.keyboard.isDown("d")
  )
  self:setPosition(changedBy(self.pos, dir:normalized(), self.speed, dt))
end


function Player:constrainToScreen()
  if self.pos.x < 0 then
    self:setPosition(0, self.pos.y)
  elseif self.pos.x > width() then
    self:setPosition(width(), self.pos.y)
  end

  if self.pos.y < 0 then
    self:setPosition(self.pos.x, 0)
  elseif self.pos.y > height() then
    self:setPosition(self.pos.x, height())
  end
end


function Player:update (dt)
  self:move(dt)
  self:constrainToScreen()
end

function Player:draw ()
  love.graphics.setColor(unpack(self.color))
  love.graphics.rectangle("fill", self.pos.x - self.dims.x / 2, self.pos.y - self.dims.y / 2, self.dims.x, self.dims.y)
end

return Player
