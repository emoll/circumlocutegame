
Enemy = {}
attachEmptyLoveCallbacks(Enemy)

function Enemy:new(obj)
  obj = obj or {}

  setmetatable(obj, self)
  self.__index = self

  obj.dims = obj.dims or vector(50, 50)

  obj.pos = obj.pos or vector(0, 0)
  obj.speed = 100 -- (px/s)

  obj.target = obj.target or {pos = vector(0, 0)} -- obj with pos vector
  obj.stopDistance = 1 -- (px)

  obj.color = obj.color or {255, 0, 0}

  obj.colRect = HC.rectangle(obj.pos.x, obj.pos.y, obj.dims.x, obj.dims.y)


  return obj
end


function Enemy:setPosition (x, y)
  if vector.isvector(x) then
    self.pos = x
  else
    self.pos.x = x
    self.pos.y = y
  end

  self.colRect:moveTo(self.pos.x, self.pos.y)
end


function Enemy:update(dt)
  local targetVec = self.target.pos - self.pos

  if targetVec:len() > self.stopDistance then
    self:setPosition(changedBy(self.pos, targetVec:normalized(), self.speed, dt))
  end
end

function Enemy:draw()
  love.graphics.setColor(unpack(self.color))
  love.graphics.rectangle("fill", self.pos.x - self.dims.x / 2, self.pos.y - self.dims.y / 2, self.dims.x, self.dims.y)
end

return Enemy
