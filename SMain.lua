
SMain = {}

function SMain:update (dt)
  SUITLayoutInitial()

  suit.layout:padding(10, 10)

  suit.layout:row()
  suit.layout:col()
  suit.Label("Circumlocute", suit.layout:col())

  if suit.Button("Play", suit.layout:row()).hit then
    gamestate.switch(SGame)
  end

  if suit.Button("Exit", suit.layout:row()).hit then
    love.event.quit()
  end
end

function SMain:draw()
  suit.draw()
end

return SMain
