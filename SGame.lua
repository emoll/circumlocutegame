
local Enemy = require "Enemy"
local Player = require "Player"


-- Debug --------------
DBG_Disable_Collisions = false

DBG_Draw = false
DBG_Lines = {}

DBG_Draw_Spawn_Lines = false


SGame = {}

-- GUI functions: ------------------
function SGame:GUIMain()
  suit.layout:reset()
  suit.layout:padding(10, 10)

  if self.isGameOver then
    suit.layout:col(SUIT_Cell_Width, SUIT_Cell_Heigth)
    suit.layout:col(SUIT_Cell_Width, SUIT_Cell_Heigth)
    suit.layout:row(SUIT_Cell_Width, SUIT_Cell_Heigths)
    GUIGameOver()
  end

  GUIScore(round(self.score, 2))

  if self.isGameOver then
    GUIreturnButton()
    GUIRestartButton()
  end
end

function GUIreturnButton ()
  if suit.Button("Return", suit.layout:row(SUIT_Cell_Width, SUIT_Cell_Heigth)).hit then
    gamestate.switch(SMain)
  end
end

function GUIGameOver ()
  suit.Label("Game Over", {align = "center", valign = "center"}, suit.layout:row(SUIT_Cell_Width, SUIT_Cell_Heigth))
end

function GUIScore(score)
  suit.Label("Score: " .. score, {align = "center", valign = "center"}, suit.layout:row(SUIT_Cell_Width, SUIT_Cell_Heigth))
end

function GUIRestartButton()
  if suit.Button("Restart", suit.layout:row(SUIT_Cell_Width, SUIT_Cell_Heigth)).hit then
    gamestate.switch(SGame)
  end
end


-- Main functions: ------------
function SGame:enter (from) -- Use this as initializer to ensure proper behaviour regardless of when state is entered
  -- print("Enter")
  self.entities = {}

  self.doUpdate = true
  self.isGameOver = false

  self.enemyTimer = 0 -- seconds
  self.enemyTargetTime = 3 -- seconds

  self.player = Player:new{pos = vector(width() / 2, height() / 2)}
  table.insert(self.entities, self.player)

  self.score = 0

  self.speedScaleFactor = 0.01;


  self:spawnEnemy() -- Start by spawing an enemy immediately
end

function SGame:clearCollisionShapes()
  for _, entity in ipairs(self.entities) do
    HC.remove(entity.colRect)
  end
end

function SGame:leave()
  -- print("Leave game")
  self:clearCollisionShapes()
end


-- Enemy stuff -------------
function SGame:spawnEnemy ()
  -- TODO: random position (radius?)
  local randVec = vector.randomDirection(Diagonal_Length / 2)
  local spawnVec = vector(width() / 2, height() / 2) + randVec
  table.insert(self.entities, Enemy:new{pos = spawnVec, target = self.player})

  if DBG_Draw_Spawn_Lines then
    table.insert(DBG_Lines, {vector(width() / 2, height() / 2), spawnVec})
  end
end

function SGame:checkSpawnEnemies (dt)
  self.enemyTimer = self.enemyTimer + dt

  if self.enemyTimer >= self.enemyTargetTime then
    self:spawnEnemy()
    self.enemyTimer = self.enemyTimer - self.enemyTargetTime
  end
end


-- Other stuff ----------------------
function SGame:gameOver ()
  self.doUpdate = false
  self.isGameOver = true
end


function SGame:checkCollisions ()
  if DBG_Disable_Collisions then
    return
  end
  for shape, delta in pairs(HC.collisions(self.player.colRect)) do
    self:gameOver()
  end
end


function SGame:updateScore (dt)
  self.score = self.score + dt
end


function SGame:increaseSpeed(dt)
  for _, entity in ipairs(self.entities) do
    entity.speed = entity.speed + (entity.speed * self.speedScaleFactor * dt)
  end
end


function SGame:update(dt)
  if self.doUpdate then
    self:checkSpawnEnemies(dt)
    self:checkCollisions()

    self:updateScore(dt)

    self:increaseSpeed(dt)

    for _, entity in ipairs(self.entities) do
      entity:update(dt)
    end
  end

  self:GUIMain()
end


function SGame:draw ()
  for _, entity in ipairs(self.entities) do
    entity:draw()
  end

  suit.draw()

  if DBG_Draw then
    for _, line in ipairs(DBG_Lines) do
      love.graphics.line(line[1].x, line[1].y, line[2].x, line[2].y)
    end
  end
end



return SGame
