
local all_callbacks = { 'draw', 'errhand', 'update' }
for k in pairs(love.handlers) do
  table.insert(all_callbacks, k)
end

-- Checks through all callbacks defined by lua, if they are not implemented
-- in obj, an empty function is inserted.
-- This is used to generalize objects so that they can be put into an array
-- together without stuff breaking for example.
function attachEmptyLoveCallbacks (obj)
  for _, callback in pairs(all_callbacks) do
    if obj[callback] == nil then
      obj[callback] = function() end
    end
  end
end


-- returns a new vector, which is the supplied vector + dir*speed*dt
-- speed and dt defaults to 1 if not supplied
function changedBy(vector, dir, speed, dt)
  speed = speed or 1
  dt = dt or 1
  return vector + dir * speed * dt
end


-- returns a (non-normalized) vector representing the direction that the
-- pressed buttons represent
function moveDirFromKeys (upPressed, downPressed, leftPressed, rightPressed)
  local dir = vector(0, 0)

  if upPressed then
    dir.y = dir.y - 1
  end
  if downPressed then
    dir.y = dir.y + 1
  end
  if leftPressed then
    dir.x = dir.x - 1
  end
  if rightPressed then
    dir.x = dir.x + 1
  end

  return dir
end


-- Rounds to numDecimalPlaces
-- rounds to 10s, 100s and so on if numDecimalPlaces is negative
function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end
